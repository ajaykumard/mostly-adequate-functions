# mostly-adequate-functions

All the functions from mostly adequate guide wrapped up into one file and one file only, easy to use. I personally do recommend to use ramda, ramda-fantasy, fluture and other often used functional libraries.

I sometimes feel these functions very comfortable to use when compared to other libraries, especially because I am a JS Functional Programming beginner and hence making them available in npm. 

Source book: Mostly Adequate Guide, https://mostly-adequate.gitbooks.io/mostly-adequate-guide/
<br/>
An excellent book, must read.

There is also the original mostly adequate guide appendix chapters etc all bundled up in an npm module https://www.npmjs.com/package/@mostly-adequate/support, have a look
